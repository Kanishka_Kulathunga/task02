
public class Location extends SpacePlace {
  public int y;
  public int x;
  public DIRECTION d;
  
  public enum DIRECTION {VERTICAL, HORIZONTAL};
  
  public Location(int r, int c) {
    this.x = r;
    this.y = c;
  }

  public Location(int r, int c, DIRECTION d) {    
    this(r,c);
    this.d=d;
  }
  
  public String toString() {
    if(d==null){
      return "(" + (y+1) + "," + (x+1) + ")";
    } else {
      return "(" + (y+1) + "," + (x+1) + "," + d + ")";
    }
  }
}
